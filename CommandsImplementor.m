#import "FighterUtils.h"
#import "CommandsImplementor.h"

#define MAX_COMMANDS_TO_IMPLEMENT 10

@implementation CommandsImplementor
{
	dispatch_queue_t implementorQueue;
	id<ActionImplementor> implementor;
}


-(id) initWithActionImplementor: (id<ActionImplementor>) impl
{
	self = [super init];

	implementorQueue = [self createDispatchQueue];
	implementor = impl;
	_commandsCount = 0;

	return self;
}

-(BOOL) canCommandBeAdded
{
	return _commandsCount < MAX_COMMANDS_TO_IMPLEMENT;
}

-(void) incCommandCount
{
	_commandsCount++;
}

-(void) decCommandCount
{
	_commandsCount--;
}

- (dispatch_queue_t) createDispatchQueue
{
	//return dispatch_get_main_queue();
	return dispatch_queue_create("implementor.queue", NULL);
	//return dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
}

-(void) makeScreenClick: (ScreenClick *) click
{
	if (click) {
		if ([self canCommandBeAdded]) {
                	[self incCommandCount];

			CGPoint point = CGPointMake([click getX], [click getY]);

                	dispatch_async(implementorQueue, ^{
                        	@autoreleasepool {
                                	[FighterUtils clickAtPointPixel: point];
					[self decCommandCount];
                        	}
                	});
		}
	}	
}


-(void) makeScreenDrag: (DragClick *) dragClick
{
        if (dragClick) {
                if ([self canCommandBeAdded]) {
                        [self incCommandCount];

                        CGPoint fromPoint = CGPointMake([dragClick getXStart], [dragClick getYStart]);
			CGPoint toPoint = CGPointMake([dragClick getXEnd], [dragClick getYEnd]);
			int durationMS = [dragClick getDelayMS];
                        
			dispatch_async(implementorQueue, ^{
                                @autoreleasepool {
					[FighterUtils dragPointPixel : fromPoint : toPoint : durationMS];
                                        [self decCommandCount];
                                }
                        });
			
                }
        }

}

-(void) pressHomeButton: (HomeClick *) home
{
	if ([self canCommandBeAdded]) {
		[self incCommandCount];

		dispatch_async(implementorQueue, ^{
			@autoreleasepool {
				[FighterUtils pressHomeButton];
				[self decCommandCount];
			}
		});
	}
}

-(void) unlock: (UnlockIPhone *) unlock
{

	if ([self canCommandBeAdded]) {
		[self incCommandCount];

		dispatch_async(implementorQueue, ^{
			@autoreleasepool {
				[FighterUtils pressUnlock];
				[self decCommandCount];
			}
		});
	}
}

-(void) reboot: (RebootIPhone *) reboot
{

	if ([self canCommandBeAdded]) {
		[self incCommandCount];

		dispatch_async(implementorQueue, ^{
			@autoreleasepool {
				[FighterUtils pressReboot];
				[self decCommandCount];
			}
		});
	}

}

-(void) showRun: (ShowRun *) showRun
{

	if ([self canCommandBeAdded]) {
		[self incCommandCount];

		dispatch_async(implementorQueue, ^{
			@autoreleasepool {
				[FighterUtils pressShowRun];
				[self decCommandCount];
			}
		});
	}
}

-(void) inputText: (TextInput *) textInput
{

	if ([self canCommandBeAdded]) {
		[self incCommandCount];

		dispatch_async(implementorQueue, ^{
			@autoreleasepool {
				[FighterUtils inputString: [textInput getText]];
				[self decCommandCount];
			}
		});
	}

}

-(void) makeCommand: (id<ICommandToImplement>) command
{
	if (command) 
	{
		if ([command class] == [ScreenClick class]) {
			ScreenClick *screen = (ScreenClick*) command;		
			[self makeScreenClick: screen];
		} else if ([command class] == [DragClick class]) {
			DragClick *drag = (DragClick*) command;
			[self makeScreenDrag: drag];
		} else if ([command class] == [HomeClick class]) {
			HomeClick *home = (HomeClick*) command;
			//[implementor stopWorking];
			[self pressHomeButton: home];
		} else if ([command class] == [RebootIPhone class]) {
			RebootIPhone *reboot = (RebootIPhone*) command;
			[self reboot: reboot];
		} else if ([command class] == [ShowRun class]) {
			ShowRun *showRun = (ShowRun*) command;
			[self showRun: showRun];
		} else if ([command class] == [UnlockIPhone class]) {
			UnlockIPhone *unlock = (UnlockIPhone*) command;
			[self unlock: unlock];
		} else if ([command class] == [ScreenshotDelay class]) {
			ScreenshotDelay *scrDelay = (ScreenshotDelay*) command;
			[implementor setScreenshotSpeed: [scrDelay getDelayMillis]];
		} else if ([command class] == [TextInput class]) {
			TextInput *textInput = (TextInput*) command;
			[self inputText: textInput];
		}
	}	
}
@end 
