@protocol ICommandToImplement<NSObject>
- (NSString*) getDescription;
@end

@interface ScreenClick: NSObject<ICommandToImplement>
+(ScreenClick*) createFromString: (NSString*) str;
-(id) initFromX : (int) x andY : (int) y;
-(int) getX;
-(int) getY;
@end

@interface DragClick: NSObject<ICommandToImplement>
+(DragClick*) createFromString: (NSString *) str;
-(id) initXFrom: (int) xFrom andYFrom : (int) yFrom andXTo : (int) xTo andYTo: (int) yTo andDurMS: (int) durMS;
-(int) getXStart;
-(int) getYStart;
-(int) getXEnd;
-(int) getYEnd;
-(int) getDelayMS;
@end

@interface HomeClick: NSObject<ICommandToImplement>
+(HomeClick*) createFromString: (NSString *)str;
@end

@interface RebootIPhone: NSObject<ICommandToImplement>
+(RebootIPhone*) createFromString: (NSString *)str;
@end

@interface ShowRun: NSObject<ICommandToImplement>
+(ShowRun*) createFromString: (NSString *)str;
@end

@interface UnlockIPhone: NSObject<ICommandToImplement>
+(UnlockIPhone*) createFromString: (NSString *)str;
@end

@interface ScreenshotDelay: NSObject<ICommandToImplement>
+(ScreenshotDelay*) createFromString: (NSString *)str;
-(id) initFrom: (int) delay;
-(int) getDelayMillis;
@end

@interface TextInput: NSObject<ICommandToImplement>
+(TextInput*) createFromString: (NSString *) str;
-(id) initFrom: (NSString *) str;
-(NSString*) getText;
@end

