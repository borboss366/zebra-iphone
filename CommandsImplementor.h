// one needs fighter utils
#import "ActionSender.h"
#import "Commands.h"

@interface CommandsImplementor: NSObject
-(id) initWithActionImplementor: (id<ActionImplementor>) implementor;
-(void) makeScreenClick: (ScreenClick *) click;
-(void) makeScreenDrag: (DragClick *) dragClick;
-(void) pressHomeButton: (HomeClick *) home;
-(void) unlock: (UnlockIPhone *) unlock;
-(void) reboot: (RebootIPhone *) reboot;
-(void) showRun: (ShowRun *) showRun;
-(void) inputText: (TextInput *) textInput;

-(void) makeCommand: (id<ICommandToImplement>) command;
@property (atomic, readonly) int commandsCount;
@end

