#import "ChainsawReceiveSocketState.h"

@implementation ChainsawReceiveSocketState
{
	ChainsawEncoderProvider *provider;
	
	NSMutableData *currentPrefixData;
	int readNextQuantSize;
	
	PrefixDescriptorObject* currentPDO;

	BOOL objectConsumed;
	
	// the data of the object
	NSMutableData *objectData;
	ChainsawReceivedObject *object;
	int size2Receive;
	

	// large object start
	int largeObjectRead;

	NSMutableData *remainingData;
}

-(id) initWithChainsawEncoderProvider:(ChainsawEncoderProvider*) encoderProvider andNSData: (NSData*) data
{
	self = [super init];
	provider = encoderProvider;

	// init the start values
	objectConsumed = NO;

	currentPDO = nil;
	size2Receive = -1;
	readNextQuantSize = [self getDefaultQuantSize]; 

	// init all data with the empty stuff
	currentPrefixData = [[NSMutableData alloc] initWithLength:0]; // init with zero data
	objectData = [[NSMutableData alloc] initWithLength:0];	
        remainingData = [[NSMutableData alloc] initWithLength:0];

	object = nil;
	largeObjectRead = 0;

	[self addNewData: data];
	return self;
}


-(NSString*) getCurrentPrefixString
{
	return [self createCPSString];
	//return currentPrefixData;
}


-(int) getDefaultQuantSize
{
	int quant = [provider getSizeLen];
	int minPrefixLen = [provider getMinPrefixLen];
	
	if (quant > minPrefixLen) {
		quant = minPrefixLen;	
	}
	
	return quant;
}

-(BOOL) isReadingPrefix
{
	BOOL pr = ![self isFinished];
	pr &= currentPDO == nil;
	return pr;
}

-(void) addNewData: (NSData*) data
{
	if (![self isFinished]) {
		if ([self isReadingPrefix]) {
			[self appendToPrefixObject: data];
		} else if ([self isObjectTooLarge]) {
			[self appendToLargeObject: data];
		} else {
			[self appendToReadingObject: data];
		}
	}
}

// this is how many bytes are received by object
-(int) getObjectReceivedDataSize
{
	return [objectData length];
}

-(void) appendToLargeObject: (NSData*) data
{
	// we received enough data to make object. But it is large so fuck it
	if (data.length + largeObjectRead >= size2Receive) {
		objectConsumed = YES;
		[self append2Data: remainingData withData: data from: (size2Receive - largeObjectRead)];		
	} else {
		// just increase the large data
		largeObjectRead += data.length;
		readNextQuantSize = size2Receive - largeObjectRead;
	}
}

-(void) appendToReadingObject: (NSData*) data 
{
	// append the data to data
	// all data received
	if (data.length + objectData.length >= size2Receive) {
		[self append2Data: objectData withData: data to: (size2Receive - objectData.length)];
		objectConsumed = YES;
		// append the remaining part
		object = [[ChainsawReceivedObject alloc] initWithPrefix: currentPDO andObjectData: objectData];
		[self append2Data: remainingData withData: data from: (size2Receive - objectData.length)];
	} else {
		[self append2Data : objectData withData: data];
		readNextQuantSize = size2Receive - objectData.length;
	}
}

-(void) appendToPrefixObject: (NSData*) data
{
	// blabla update the data
	[self append2Data: currentPrefixData withData: data];

	// init the current prefix string
	NSString *cps = [self createCPSString];
	ChainsawEncoderProviderSearchResult *res = [provider findPrefix: cps];	
	
	// yay we found prefix
	if ([res isFound]) {
		[self processFoundPrefix: res andStr: cps];
	} else {
		[self truncateUnneededPrefixData];			
	}
}

-(void) processFoundPrefix: (ChainsawEncoderProviderSearchResult*) sr andStr: (NSString*) cps
{
	int pos = [sr getSearchPosition];
	int objFrom = pos + [provider getSizeLen] + [[sr getPDO] getPrefix].length;

	if (objFrom <= cps.length) {
		// update the current pdo
                currentPDO = [sr getPDO];
                size2Receive = [self parseSize: cps andPosition: pos + [currentPDO getPrefix].length];

		// whops the negative size, chop from the end of the obj
                if (size2Receive <= 0) {
			// finished here
			[self append2Data: remainingData withData: currentPrefixData from: objFrom];
                } else if ([self isObjectTooLarge]) {
			largeObjectRead = cps.length - objFrom;			
			[self appendToLargeObject: [NSData data]];
		} else {
                        [self append2Data: objectData withData: currentPrefixData from: objFrom];
                        // launch object calculation
			[self appendToReadingObject: [NSData data]];
		}
	} else {
		// calculate the next quant size
		readNextQuantSize = objFrom - cps.length;	
	}
}

-(int) parseSize: (NSString*) from andPosition:(int) pos
{
	NSRange range = NSMakeRange(pos, [provider getSizeLen]);
	NSString *sizeString = [from substringWithRange: range];
	return sizeString.integerValue;
}

-(void) append2Data: (NSMutableData*) mData withData: (NSData*) data
{
	[mData appendData: data];
}

-(void) append2Data: (NSMutableData*) mData withData: (NSData*) data to: (int) pos
{
        NSRange range = NSMakeRange(0, pos);
        NSData *partData = [data subdataWithRange: range];
        [self append2Data :mData withData: partData];
}

-(void) append2Data: (NSMutableData*) mData withData: (NSData*) data from: (int) pos
{
	NSRange range = NSMakeRange(pos, data.length - pos);
        NSData *partData = [data subdataWithRange: range];
	[self append2Data :mData withData: partData];
}

-(void) truncateUnneededPrefixData
{
        // chop the unneded data leave the size of
	if (currentPrefixData.length > [provider getMinLength2Mantain]) {
		NSRange range = NSMakeRange(currentPrefixData.length - [provider getMinLength2Mantain], [provider getMinLength2Mantain]);
		NSData *subData = [currentPrefixData subdataWithRange: range];
		currentPrefixData = [NSMutableData dataWithLength:0];
		[self append2Data: currentPrefixData withData: subData];
	}
}

-(NSString*) createCPSString
{
	return [[NSString alloc] initWithData: currentPrefixData encoding:NSASCIIStringEncoding];
}

-(int) getReadQuant
{
	return readNextQuantSize;
}

-(BOOL) isObjectParsed
{
	return objectConsumed;
}

-(ChainsawReceivedObject*) getReceivedObject
{
	return object;	
}

-(BOOL) isObjectTooLarge
{
	return size2Receive > [provider getMaxReceiveObjectSize];
}

-(BOOL) isObjectFailed
{
	return currentPDO != nil && size2Receive <= 0;
}

-(NSData*) getRemainingData
{
	return remainingData;
}

-(BOOL) isFinished
{
	BOOL res = NO;

	res |= [self isObjectFailed];
	res |= [self isObjectParsed];

	return res;
}

@end
