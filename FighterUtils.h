@interface FighterUtils: NSObject

// returns the id of the current phone. It is the alphanumeric string 
+ (NSString *) getIPhoneId;

// clicks at the specified point
+ (void) clickAtPointPoint :(CGPoint) clickAtPoint;

// Drags to the specified point
+ (void) dragPointPoint :(CGPoint) fromPoint :(CGPoint) toPoint :(int) durationMS;

+ (void) clickAtPointPixel: (CGPoint) clickAt;

+ (void) dragPointPixel :(CGPoint) fromPoint :(CGPoint) toPoint :(int) durationMS;

+ (void) pressHomeButton;

+ (void) pressUnlock;
+ (void) pressReboot;
+ (void) pressShowRun;

+ (void) inputString :(NSString *) str;
+ (void) inputChar :(char) c; 
+ (int) calcKeyCode :(char) c :(char) f :(int) startIndex;


+ (int) getBatteryChargeValue;

+ (unsigned long) getCurrentTimeMS;

+ (CGImageRef) captureMyScreen;

@end
