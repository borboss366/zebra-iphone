#import "ChainsawSocket.h"


@implementation ChainsawServer {
	dispatch_queue_t socketQueue; // the socket queue this socket listener is bound to
	dispatch_queue_t listenerQueue;
	GCDAsyncSocket *listenSocket; // the listen socket
	
	GCDAsyncSocket *activeSocket;
	ChainsawReceiveSocketState *socketState;
	
	// int lastTag;
	// int lastTagWritten;

	//NSData *lastDataReceived;
	//NSString *lastStrReceived;

	int objectsProcessed;	

	//NSMutableArray *connectedSockets; // the array of currently connected sockets
	//NSMutableArray *socketStates; // the dictionary of socket states for receivng the objects

	ChainsawEncoderProvider *encoder;

	
	BOOL running;
	NSMutableArray *listeners;
}

- (id) initWithChainsawEncoder: (ChainsawEncoderProvider *) encoderProvider
{
	return [self initWithChainsawEncoder: encoderProvider andQueue: dispatch_get_main_queue()];
	/*
	self = [super init];

	if (self) {
		encoder = encoderProvider;

		// make the queue
		socketQueue = dispatch_queue_create("socketQueue", NULL);
		listenSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:socketQueue];
		
		objectsProcessed = 0;

		activeSocket = nil;
		socketState = nil;
		//lastTag = 0;
		//lastTagWritten = 0;

		//lastDataReceived = nil;
		//lastStrReceived = nil;

		//connectedSockets = [[NSMutableArray alloc] initWithCapacity:1];
		//socketStates = [[NSMutableArray alloc] initWithCapacity:1];

		listeners = [[NSMutableArray alloc] initWithCapacity:10];	
		running = NO;
	}
	
	return self;
	*/
}

- (id) initWithChainsawEncoder: (ChainsawEncoderProvider *) encoderProvider andQueue: (dispatch_queue_t) queue
{
        self = [super init];

        if (self) {
                encoder = encoderProvider;
		listenerQueue = queue;


                // make the queue
                socketQueue = dispatch_queue_create("socketQueue", NULL);
                listenSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:socketQueue];

                objectsProcessed = 0;

                activeSocket = nil;
                socketState = nil;

                listeners = [[NSMutableArray alloc] initWithCapacity:10];
                running = NO;
        }

        return self;
}


- (int) getPort
{
	return 666;
}

- (void) startSocketServer
{
	dispatch_async(socketQueue, ^{
		@autoreleasepool {
			[self startLocalSocketServer];
		}
	});
}

-(void) startLocalSocketServer
{
        if(!running)
        {
                int port = [self getPort];
                NSError *error = nil;

                if([listenSocket acceptOnPort:port error:&error])
                {
                        running = YES;
                        [self notifyServerStateChanged];
                }
        }
}

- (void) stopSocketServer
{
        dispatch_async(socketQueue, ^{
                @autoreleasepool {
                        [self stopLocalSocketServer];
                }
        });
}

-(void) stopLocalSocketServer
{
	if (running) {
                [listenSocket disconnect];
                [self clearSockets];
                running = NO;
                [self notifyServerStateChanged];
        }
}

- (BOOL) isServerRunning
{
	return running;
}

-(void) flushSocketServer
{
        dispatch_async(socketQueue, ^{
                @autoreleasepool {
                        [self flushLocalSocketServer];
                }
        });

}

-(void) flushLocalSocketServer
{
	if (activeSocket) {
		socketState = [self createBrandNewSocketState];
		[self notifyServerStateChanged];
	}
}

-(void) addObjectForSending: (ChainsawReceivedObject*) object
{
	dispatch_async(socketQueue, ^{
		@autoreleasepool {
			if (activeSocket) {
				[self writeObject: object toSocket: activeSocket];
			}
		}
	});
}


-(BOOL) canBeWrittenToSocket: (GCDAsyncSocket*) socket
{
	return YES;
}

-(NSData*) getDataForSize: (int) size
{
	int sizeLen = [encoder getSizeLen];
	NSString *strRes = [NSString stringWithFormat:@"%d", size];
	
	if ([strRes length] < sizeLen) {
		int add = sizeLen - [strRes length];
		NSString *pad = [@"" stringByPaddingToLength:add withString:@" " startingAtIndex:0];
		strRes = [pad stringByAppendingString: strRes];
	}

	return [strRes dataUsingEncoding:NSASCIIStringEncoding];
}

-(int) getNextTag
{
	return arc4random_uniform(1000 * 1000);
}

-(void) writeObject: (ChainsawReceivedObject*) object toSocket: (GCDAsyncSocket *) socket
{
	if (object && [self canBeWrittenToSocket: socket])   {
		int tag = [self getNextTag];
		NSData *prefixData = [[object getPrefixDescriptorObject] getPrefixData];
		NSData *sizeData = [self getDataForSize: [object getObjectLength]];
		NSData *objectData = [object getObjectData];
                [socket writeData:prefixData withTimeout:-1 tag:tag];
                [socket writeData:sizeData withTimeout:-1 tag:tag];
		[socket writeData:objectData withTimeout:-1 tag:tag];
	}
}

-(int) getObjectsProcessed
{
	return objectsProcessed;
}	

-(void) clearSockets
{
	if (activeSocket) {
		[activeSocket disconnect];
		activeSocket = nil;
		socketState = nil;
		//lastTag = 0;
		//lastTagWritten = 0;
		[self notifyServerStateChanged];
	}
}

-(int) getConnectedSocketsCount
{
	int res = 0;

	if (activeSocket) {	
		res = 1;	
	}		

	return res;
}

- (ChainsawReceiveSocketState *) createBrandNewSocketState
{
	return [self createBrandNewSocketStateWithData: [NSData data]];
}

- (ChainsawReceiveSocketState *) createBrandNewSocketStateWithData: (NSData *) data
{
        return [[ChainsawReceiveSocketState alloc] initWithChainsawEncoderProvider: encoder andNSData: data];
} 

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
	// This method is executed on the socketQueue (not the main thread)
	if (activeSocket) {
		[activeSocket disconnect];
	}


	activeSocket = newSocket;
	
	socketState = [self createBrandNewSocketState];
	int readQuant = [socketState getReadQuant];
	
	[self writeHelloData: activeSocket];       
	[self performReading: activeSocket byteNum: readQuant];
	[self notifyNewSocketConnected];
}

-(void) writeHelloData: (GCDAsyncSocket*) sock
{

        NSString *welcomeMsg = @"Sick my duck\r\n";
        NSData *welcomeData = [welcomeMsg dataUsingEncoding:NSASCIIStringEncoding];
        [sock writeData:welcomeData withTimeout:-1 tag:0];
}

-(void) performReading: (GCDAsyncSocket *)sock byteNum: (int) quant 
{
	[sock readDataToLength:quant withTimeout: -1 tag: 0];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
	// If we got the message not from the listening socket, remove from the connected sockets
	if (sock != listenSocket)
	{	
		activeSocket = nil;
		socketState = nil;
		//lastTag = 0;
		//lastTagWritten = 0;
		[self notifySocketDisconnected];
	}
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{	
	// if the socket is active socket
	if (activeSocket == sock) {		
		ChainsawReceiveSocketState *newState = [self updateState: socketState withData : data];

		if (newState) {
			socketState = newState;
		}

		int quant = [socketState getReadQuant];
		[self performReading: activeSocket byteNum: quant];
	}
}

-(ChainsawReceiveSocketState*) updateState: (ChainsawReceiveSocketState*) state withData: (NSData*) data
{
	ChainsawReceiveSocketState* newState = nil;
	[state addNewData: data];

	if ([state isFinished]) {
		// notify new object
		objectsProcessed++;
	
		if ([state isObjectParsed] && ![state isObjectTooLarge]) {
	                ChainsawReceivedObject *newObject = [state getReceivedObject];
			[self notifyNewObjectReceived: newObject];
		}	

		NSData *newData = [state getRemainingData];
		newState = [self createBrandNewSocketStateWithData: newData];
	}

	return newState;
}	

- (void) notifyNewSocketConnected
{
	@synchronized(listeners) 
	{
	        for (int i = 0; i < [listeners count]; i++)
                {
			id<IGCDSocketServerListener> listener = [listeners objectAtIndex:i];

			dispatch_async(listenerQueue, ^{
                		@autoreleasepool {
                			[listener newSocketConnected];
				}
        		});
                }
     		
	}
}



- (void) notifyNewObjectReceived: (ChainsawReceivedObject*) object;
{
        @synchronized(listeners) 
        {
                for (int i = 0; i < [listeners count]; i++)
                {
                        id<IGCDSocketServerListener> listener = [listeners objectAtIndex:i];

                        dispatch_async(listenerQueue, ^{
                                @autoreleasepool {
                                        [listener objectReceived: object];
                                }
                        });
                }

        }
}



- (void) notifyServerStateChanged
{
        @synchronized(listeners)
        {
                for (int i = 0; i < [listeners count]; i++)
                {
                        id<IGCDSocketServerListener> listener = [listeners objectAtIndex:i];

                        dispatch_async(listenerQueue, ^{
                                @autoreleasepool {
                                        [listener serverStateChanged];
                                }
                        });
                }
        }

}

- (void) notifySocketDisconnected
{
        @synchronized(listeners)
        {
                for (int i = 0; i < [listeners count]; i++)
                {
                        id<IGCDSocketServerListener> listener = [listeners objectAtIndex:i];

                        dispatch_async(listenerQueue, ^{
                                @autoreleasepool {
                                        [listener socketDisconnected];
                                }
                        });
                }
        }
}

// Listeners should be synchronized on themselves
- (void) addServerListener: (id<IGCDSocketServerListener>) listener
{
        @synchronized(listeners) {
                [listeners addObject: listener];
        }
}

@end


/*
- (void) notifySocketStateChanged: (ChainsawReceiveSocketState*) state;
{
        @synchronized(listeners)
        {
                for (int i = 0; i < [listeners count]; i++)
                {
                        id<IGCDSocketServerListener> listener = [listeners objectAtIndex:i];

                        dispatch_async(dispatch_get_main_queue(), ^{
                                @autoreleasepool {
                                        [listener socketStateChanged: state recvDataSize: 0];
                                }
                        });
                }
        }
}
*/



/*
- (ChainsawReceiveSocketState*) getSocketState
{
        return socketState;
}
*/
/*
-(int) getLastDataSizeRead
{
        int res = 0;

        if (lastDataReceived) {
                res = [lastDataReceived length];
        }

        return res;
}


-(NSString*) getLastStringReceived
{
        NSString *res = @"";

        if (lastStrReceived) {
                res = lastStrReceived;
        }

        return res;
}
*/
/*
-(void) flushSocketServer
{
        @synchronized(connectedSockets) {
                for (int i = 0; i < [connectedSockets count]; i++) {
                        ChainsawReceiveSocketState *state = [self createBrandNewSocketState];
                        [socketStates replaceObjectAtIndex:i withObject: state];
                        [self notifySocketStateChanged: state];
                }
        }
}
*/


/*
-(void) clearSockets
{
        @synchronized(connectedSockets) {
                for (int i = 0; i < [connectedSockets count]; i++)
                {
                        [[connectedSockets objectAtIndex:i] disconnect];
                }

                [connectedSockets removeAllObjects];
                [socketStates removeAllObjects];
        }
}
*/

/*
- (int) getConnectedSocketsCount
{
        int res = 0;

        @synchronized(connectedSockets) {
                res = [connectedSockets count];
        }

        return res;
}
*/

       /*
        @synchronized(connectedSockets)
        {
                [connectedSockets addObject:newSocket];
                socketState = [self createBrandNewSocketState];
                [socketStates addObject: socketState];
        }

        [self notifyNewSocketConnected];
        [self notifySocketStateChanged: socketState];

               //socketState = [self createBrandNewSocketState];
                //int readQuant = [socketState getReadQuant];
 
        int readQuant = [socketState getReadQuant];
        [self performReading: sock byteNum: readQuant];
        */
        //NSString *host = [newSocket connectedHost];
        //UInt16 port = [newSocket connectedPort];
        //NSString *welcomeMsg = @"Sick my duck\r\n";
        //NSData *welcomeDacta = [welcomeMsg dataUsingEncoding:NSASCIIStringEncoding];
        //[newSocket writeData:welcomeData withTimeout:-1 tag:0];
               /*
                ChainsawReceiveSocketState *socketState = nil;

                @synchronized(connectedSockets)
                {
                        int index = [connectedSockets indexOfObject:sock];
                        [connectedSockets removeObject:sock];
                        socketState = [socketStates objectAtIndex: index];
                        [socketStates removeObjectAtIndex: index];
                }

                [self notifySocketDisconnected];
                [self notifySocketStateChanged: socketState];
                */


/*
-(void) updateLastReadString:(NSData *) data
{
        NSString *strRec = [[NSString alloc] initWithData: data encoding:NSASCIIStringEncoding];
        int maxLen = 15;
        if (strRec.length > maxLen) {
                strRec = [strRec substringFromIndex:(strRec.length - maxLen)];
        }

        lastStrReceived = strRec;
}
*/



        /*
        ChainsawReceiveSocketState *socketState = nil;

        @synchronized(connectedSockets)
        {
                int index = [connectedSockets indexOfObject:sock];
                socketState = [socketStates objectAtIndex: index];
                ChainsawReceiveSocketState *newState = [self updateState: socketState withData: data];
                int quant = [socketState getReadQuant];

                if (newState) {
                        [socketStates replaceObjectAtIndex:index withObject: newState];
                        quant = [newState getReadQuant];
                }

                [self performReading: sock byteNum: quant];
        }
        */

