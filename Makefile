include theos/makefiles/common.mk

#THEOS_DEVICE_IP=192.168.1.230
#THEOS_DEVICE_IP=192.168.1.9

ARCHS = armv7
ADDITIONAL_OBJCFLAGS = -fobjc-arc

TWEAK_NAME = scrmaher

scrmaher_LIBRARIES = MobileGestalt simulatetouch iokit activator simulatekeyboard

scrmaher_FILES = Tweak.xm GCDAsyncSocket.m FighterUtils.m ActionMaher.m ChainsawReceivedObject.m PrefixDescriptorObject.m ChainsawEncoder.m ChainsawReceivedObject.m ChainsawReceiveSocketState.m CommandsDecoder.m Commands.m ChainsawSocket.m ChainsawReceivedObjectCreator.m CommandsImplementor.m SocketServerAdapter.m ActivatorScrmaherSource.m

scrmaher_FRAMEWORKS = Security CFNetwork UIKit CoreGraphics QuartzCore
scrmaher_PRIVATE_FRAMEWORKS = IOSurface

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
