#import "PrefixDescriptorObject.h"

@interface ChainsawEncoderProviderSearchResult: NSObject
-(id) initWithPDO : (PrefixDescriptorObject*) pdo andIndex: (int) index andPosition: (int) position andWhere : (NSString*) where;

-(BOOL) isFound;
-(int) getPDOIndex;
-(PrefixDescriptorObject*) getPDO;
-(NSString*) getWhere2Search;
-(int) getSearchPosition;

@end

@interface ChainsawEncoderProvider: NSObject

// This method creates the default encoder provider
+(ChainsawEncoderProvider*) createDefaultEncoderProvider;

-(int) getMaxPrefixLen;
-(int) getMinPrefixLen;


-(int) getSizeLen;
-(int) getMinLength2Mantain;
-(int) getPrefixCount;
-(int) getMaxReceiveObjectSize;
-(PrefixDescriptorObject*) getPrefixAtIndex: (int) index;
-(void) addPrefixDescriptorObject: (PrefixDescriptorObject*) pdo;
-(ChainsawEncoderProviderSearchResult*) findPrefix: (NSString*) where;

@end
