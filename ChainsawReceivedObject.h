#import "PrefixDescriptorObject.h"

@interface ChainsawReceivedObject: NSObject
-(id) initWithPrefix: (PrefixDescriptorObject *) pdo andObjectData : (NSData*) data;

-(PrefixDescriptorObject*) getPrefixDescriptorObject;
-(int) getObjectLength;
-(NSData *) getObjectData;
-(NSString *) getStartDataString;
-(NSString *) getObjectString;
@end

