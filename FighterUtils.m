#import "FighterUtils.h"
#import "MobileGestalt.h"
#import "SimulateTouch.h"
#import "SimulateKeyboard.h"
#import "ActivatorScrmaherSource.h"

#import <IOSurface/IOSurface.h>
#import <sys/time.h>

#define MAX_DRAG_DURATION_MS 1000

@implementation FighterUtils 
{

}

+ (NSString*) getIPhoneId 
{
	NSString *serial = (__bridge_transfer NSString *)MGCopyAnswer(kMGSerialNumber);
	return serial;
}

+ (void) clickAtPointPoint: (CGPoint) clickAt
{
	int r = [SimulateTouch simulateTouch:0 atPoint:clickAt withType:STTouchDown];
	[SimulateTouch simulateTouch:r atPoint:clickAt withType:STTouchUp];
}

+ (void) dragPointPoint :(CGPoint) fromPoint :(CGPoint) toPoint :(int) durationMS
{
	if (durationMS > MAX_DRAG_DURATION_MS) {
		durationMS = MAX_DRAG_DURATION_MS;
	}

	if (durationMS < 0) {
		durationMS = 0;
	}

	float duration = (durationMS + 0.0) / 1000;
	[SimulateTouch simulateSwipeFromPoint:fromPoint toPoint:toPoint duration:duration];

	// this one is bettar
	[NSThread sleepForTimeInterval: (duration + 0.05f)]; 
	//CFRunLoopRunInMode(kCFRunLoopDefaultMode , duration+0.1f, NO);
}

+(void) clickAtPointPixel: (CGPoint) clickAt
{
	[FighterUtils clickAtPointPoint: [FighterUtils transformPixelToPoint: clickAt]];
}

+ (void) dragPointPixel :(CGPoint) fromPoint :(CGPoint) toPoint :(int) durationMS
{	
	[FighterUtils dragPointPoint: 
		[FighterUtils transformPixelToPoint: fromPoint] : 
		[FighterUtils transformPixelToPoint: toPoint] : durationMS];
}

+(float) getScaleFactor
{
	return 0.5f;
}

+(CGPoint) transformPixelToPoint: (CGPoint) point
{
	return CGPointMake(point.x * [FighterUtils getScaleFactor], point.y * [FighterUtils getScaleFactor]);
}

+ (void) pressUnlock
{
	LAEvent *event = [LAEvent eventWithName:UNLOCK_NAME mode:[LASharedActivator currentEventMode]];
	[LASharedActivator sendEventToListener:event];
}

+ (void) pressReboot
{
	LAEvent *event = [LAEvent eventWithName:REBOOT_NAME mode:[LASharedActivator currentEventMode]];
	[LASharedActivator sendEventToListener:event];
}

+ (void) pressShowRun
{
	LAEvent *event = [LAEvent eventWithName:SHOW_RUN_NAME mode:[LASharedActivator currentEventMode]];
	[LASharedActivator sendEventToListener:event];
}


+ (void) pressHomeButton
{	
	// This is shit, must be replaced by the hook in the application
	//[[UIApplication sharedApplication] performSelector:@selector(suspend)];
	
	LAEvent *event = [LAEvent eventWithName:HOME_NAME mode:[LASharedActivator currentEventMode]];
	[LASharedActivator sendEventToListener:event];
}

+ (int) getBatteryChargeValue
{
	NSString *battery = (__bridge_transfer NSString *)MGCopyAnswer(kMGBatteryCurrentCapacity);
	float batteryValue = [battery floatValue];
	return (int) batteryValue;
} 

+ (unsigned long) getCurrentTimeMS
{
	struct timeval time;
	gettimeofday(&time, NULL);
	unsigned long millis = (time.tv_sec * 1000) + (time.tv_usec / 1000);	
	return millis;
}

+ (void) inputString: (NSString*) str
{
	unsigned int len = [str length];
	for (int i = 0; i < len; i++) {
		[FighterUtils inputChar: [str characterAtIndex:i]];
	}
} 

+ (void) inputChar: (char) c 
{
	int code = -1;
	int secondCode = -1;
	
	if (c == '@') {
		secondCode = 229;
		code = 31; // just a guess
	} else if (c == ' ') {
		//#define KEY_SPACE 44
		code = 44;
	} else if (c == ';') {
		//#define KEY_SEMICOLON 51 // ; :
		code = 51;
	} else if (c == ',') {
		// #define KEY_COMMA 54 // , <
		code = 54;
	} else if (c == '.') {
		//#define KEY_DOT 55 // . >
		code = 55;
	} else if (c == ':') {
		code = 51;
	} else if (c == ')') {
		//#define KEY_BRACKETCLOSE 48 // ] }
		code = 48;
	} else if (c == '-') {
		#define KEY_HYPHEN 45 // - _
		code = 45;
	} else if (c == '0') {
		// the code for '0' is 39
		code = 39;
	} else if (c >= '1' && c <= '9') {
		// the code for '1' is 30 the code for '9' is 38 
		code = [FighterUtils calcKeyCode:c:'1':30];
	} else if (c >= 'a' && c <= 'z') {
		// the code for 'a' is 4 the code for z is 29
		code = [FighterUtils calcKeyCode:c:'a':4];
	} else if (c >= 'A' && c <= 'Z') {
		secondCode = 229; // leftshift
		code = [FighterUtils calcKeyCode:c:'A':4];
	} else if (c == '\n') {
		code = 40;
		// this is enter character
		// define KEY_RETURN 40
	} else if (c == '\b') {
		code = 42;
		// this is enter character
		// define KEY_BACKSPACE 42
		// this is backspace character
	}

	// the code is not null
	if (code > 0) {
		if (secondCode > 0) {
			simulateKeyboardEvent(7, secondCode, YES);
		}
		
		simulateKeyboardEvent(7, code, YES);
		simulateKeyboardEvent(7, code, NO);
		
		if (secondCode > 0) {
			simulateKeyboardEvent(7, secondCode, NO);
		}
	}
}

+ (int) calcKeyCode: (char) c : (char) f : (int) startIndex
{
	return ((int) c) - ((int) f) + startIndex;
}

void CARenderServerRenderDisplay(kern_return_t a, CFStringRef b, IOSurfaceRef surface, int x, int y);

+(CGImageRef) captureMyScreen
{
	int _bytesPerRow;
	int _width;
	int _height;

	CGRect screenRect = [UIScreen mainScreen].bounds;
	float scale = [UIScreen mainScreen].scale;

	// setup the width and height of the framebuffer for the device
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		// iPhone frame buffer is Portrait
		_width = screenRect.size.width * scale;
		_height = screenRect.size.height * scale;
	} else {
		// iPad frame buffer is Landscape
		_width = screenRect.size.height * scale;
		_height = screenRect.size.width * scale;
	}

	// Pixel format for Alpha Red Green Blue
	unsigned pixelFormat = 0x42475241;//'ARGB';

	// 4 Bytes per pixel
	int bytesPerElement = 4;

	// Bytes per row
	_bytesPerRow = (bytesPerElement * _width);
	int size = _bytesPerRow * _height;

        CFMutableDictionaryRef dict;
	dict = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        CFDictionarySetValue(dict, kIOSurfaceIsGlobal, kCFBooleanTrue);

        CFNumberRef kIOSurfaceBytesPerRowNumber = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &_bytesPerRow);
        CFDictionarySetValue(dict, kIOSurfaceBytesPerRow, kIOSurfaceBytesPerRowNumber);

        CFNumberRef kIOSurfaceBytesPerElementNumber = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &bytesPerElement);
        CFDictionarySetValue(dict, kIOSurfaceBytesPerElement, kIOSurfaceBytesPerElementNumber);

        CFNumberRef kIOSurfaceWidthNumber = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &_width);
        CFDictionarySetValue(dict, kIOSurfaceWidth, kIOSurfaceWidthNumber);

        CFNumberRef kIOSurfaceHeightNumber = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &_height);
        CFDictionarySetValue(dict, kIOSurfaceHeight, kIOSurfaceHeightNumber);

        CFNumberRef kIOSurfacePixelFormatNumber = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &pixelFormat);
        CFDictionarySetValue(dict, kIOSurfacePixelFormat, kIOSurfacePixelFormatNumber);

        CFNumberRef kIOSurfaceAllocSizeNumber = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &size);
        CFDictionarySetValue(dict, kIOSurfaceAllocSize, kIOSurfaceAllocSizeNumber);


	// Properties include: SurfaceIsGlobal, BytesPerElement, BytesPerRow, SurfaceWidth, SurfaceHeight, PixelFormat, SurfaceAllocSize (space for the entire surface)

	/*
	NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithBool:YES], kIOSurfaceIsGlobal,
                                [NSNumber numberWithInt:bytesPerElement], kIOSurfaceBytesPerElement,
                                [NSNumber numberWithInt:_bytesPerRow], kIOSurfaceBytesPerRow,
                                [NSNumber numberWithInt:_width], kIOSurfaceWidth,
                                [NSNumber numberWithInt:_height], kIOSurfaceHeight,
                                [NSNumber numberWithUnsignedInt:pixelFormat], kIOSurfacePixelFormat,
                                [NSNumber numberWithInt:_bytesPerRow * _height], kIOSurfaceAllocSize,
                                nil];
	*/
	// This is the current surface
	IOSurfaceRef _surface = IOSurfaceCreate(dict);

	IOSurfaceLock(_surface, 0, nil);
	// Take currently displayed image from the LCD
	CARenderServerRenderDisplay(0, CFSTR("LCD"), _surface, 0, 0);
	// Unlock the surface
	IOSurfaceUnlock(_surface, 0, 0);

	// Make a raw memory copy of the surface
	void *baseAddr = IOSurfaceGetBaseAddress(_surface);
	int totalBytes = _bytesPerRow * _height;

	// void *rawData = malloc(totalBytes);
	// memcpy(rawData, baseAddr, totalBytes);
	NSMutableData * rawDataObj = nil;
	rawDataObj = [NSMutableData dataWithBytes:baseAddr length:totalBytes];

	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, rawDataObj.bytes, rawDataObj.length, NULL);
	CGImageRef cgImage=CGImageCreate(_width, _height, 8,
                                     8*4, _bytesPerRow,
                                     CGColorSpaceCreateDeviceRGB(), kCGImageAlphaNoneSkipFirst |kCGBitmapByteOrder32Little,
                                     provider, NULL,
                                     YES, kCGRenderingIntentDefault);
	// CGImageRelease(cgImage);
	CFRelease(_surface);

	// releasing the dictionary stuff
	CFRelease(dict);
        CFRelease(kIOSurfaceBytesPerRowNumber);
        CFRelease(kIOSurfaceBytesPerElementNumber);
        CFRelease(kIOSurfaceWidthNumber);
        CFRelease(kIOSurfaceHeightNumber);
        CFRelease(kIOSurfacePixelFormatNumber);
        CFRelease(kIOSurfaceAllocSizeNumber);


	return cgImage;
}

@end
