@protocol ActionSender
- (void) sendScreenshot: (UIImage *) screenshot;
- (void) sendBatteryLeft: (int) batteryLeft;
- (void) sendId: (NSString*) idString;
- (void) sendTimeSinceStarted: (long) timeSinceStarted;
- (void) sendRunningStateChanged: (BOOL) newRunningState;
@end


@protocol ActionImplementor
- (void) startWorking;
- (void) stopWorking;
- (BOOL) isWorking;
- (void) setScreenshotSpeed: (int) screenshotSpeed;
- (int) getScreenshotSpeed;
- (void) addActionSender: (id<ActionSender>) sender;
@end
