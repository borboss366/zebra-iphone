#define IMAGE_PREFIX @"qOOVUcejjl3ZV1ceK7ZS"
#define CLICK_PREFIX @"EwOf9qV23DfjIub6pvyw"
#define DRAG_PREFIX @"sj2ASDGw86LenNuhsjau"
#define HOME_PREFIX @"fRrSmw8s9U0gRgonBaah"
#define ID_PREFIX @"dSM24n5jCmJK1ZUPzgQm"
#define SET_SCREENSHOT_SPEED_PREFIX @"fuyIQfp5RKbER406g8ij"
#define BATTERY_VALUE_PREFIX @"kbmAdJt6yYkmeHLB4vnh"
#define WORKING_TIME_PREFIX @"nxqtF9dUmTfXG8ObJSuB"
#define REBOOT_PREFIX @"NtZK169CVWNZf3x6uXv0"
#define UNLOCK_PREFIX @"TX8z4ZDSS5pHYV9cWgVt"
#define SHOW_RUN_PREFIX @"peIAGdprIUuJPlKmuDG6"
#define TEXT_PREFIX @"awLwfOhiATIuNPrRI0dH"

// the size of the size section
#import "ChainsawReceiveSocketState.h"
#import "ChainsawReceivedObject.h"

@protocol IGCDSocketServerListener
- (void) newSocketConnected;
- (void) serverStateChanged;
- (void) socketDisconnected;
- (void) objectReceived: (ChainsawReceivedObject*) object;
@end

@protocol IGCDSocketServer
- (void) startSocketServer;
- (void) stopSocketServer;
- (BOOL) isServerRunning;
- (int) getConnectedSocketsCount;
- (void) flushSocketServer;
- (int) getObjectsProcessed;
- (void) addObjectForSending: (ChainsawReceivedObject*) object;


// adds the listener to the server
- (void) addServerListener: (id<IGCDSocketServerListener>) listener;
@end 

//- (ChainsawReceiveSocketState*) getSocketState;
//- (int) getLastDataSizeRead;
//- (NSString*) getLastStringReceived;


