#import "SocketServerAdapter.h"


@implementation SocketServerAdapter {
	CommandsDecoder *cmdDecoder;
	CommandsImplementor *cmdImpl;
	ActionMaher *cmdMaher;
}

-(id) initWithCommandsImplementor: (CommandsImplementor*) impl andDecoder: (CommandsDecoder*) decoder 
	andMaher: (ActionMaher*) maher;
{
	self = [super init];
	cmdDecoder = decoder;
	cmdImpl = impl;
	cmdMaher = maher;
	return self;
}

- (void) newSocketConnected
{
	[cmdMaher syncIdASAP];
}

- (void) serverStateChanged
{
}

- (void) socketDisconnected
{
}

- (void) performServerUpdate
{
}

- (void) objectReceived: (ChainsawReceivedObject*) object
{
	id<ICommandToImplement> command = [cmdDecoder createCommandFromObject: object];
	[cmdImpl makeCommand: command];
}

@end

