#import "ChainsawReceivedObject.h"

#define MAX_STRING_LENGTH 25

@implementation ChainsawReceivedObject 
{
	PrefixDescriptorObject *prefixDescriptor;
	NSData *objectData;
}

-(id) initWithPrefix: (PrefixDescriptorObject *) pdo andObjectData : (NSData*) data
{
	self = [super init];
	prefixDescriptor = pdo;
	objectData = data;
	return self;
}

-(PrefixDescriptorObject*) getPrefixDescriptorObject
{
	return prefixDescriptor;
}

-(int) getObjectLength
{
	return [objectData length];
}


-(NSData *) getObjectData 
{
	return objectData;
}

-(NSString *) getStartDataString
{
	NSString *res = @"";
	
	if ([self getObjectLength] > MAX_STRING_LENGTH) {
		res = @"very fuckin long";
	} else {
		res = [[NSString alloc] initWithData: objectData encoding:NSASCIIStringEncoding];
	}

	return res;
}

-(NSString *) getObjectString
{
	return [[NSString alloc] initWithData: objectData encoding:NSASCIIStringEncoding];
}

@end
            
