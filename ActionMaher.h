#import "ActionSender.h"

#define MIN_SCREENSHOT_SPEED 100
#define MAX_SCREENSHOT_SPEED 5000
#define BATTERY_SPEED 5000
#define ID_SPEED 5000
#define TIME_PASSED_SPEED 5000

@interface ActionMaher: NSObject<ActionImplementor>
- (id) init;
- (void) addActionSender: (id<ActionSender>) actionSender;
- (void) syncIdASAP;
@end
