#import "ChainsawEncoder.h"
#import "Chainsaw.h"

#define SIZE_DEFAULT_SECTION 10
#define MAX_RECEIVE_OBJECT_SIZE 100000


@implementation ChainsawEncoderProviderSearchResult
{
	PrefixDescriptorObject *pdobject;
	int pdoIndex;
	int searchPosition;
	NSString *where2Search;
}

-(id) initWithPDO : (PrefixDescriptorObject*) pdo andIndex: (int) index andPosition: (int) position andWhere : (NSString*) where
{
	self = [super init];
	
	pdobject = pdo;
	pdoIndex = index;
	searchPosition= position;
	where2Search = where;
	
	return self;
}

-(BOOL) isFound
{
	return pdoIndex >= 0;
}

-(int) getPDOIndex 
{
	return pdoIndex;
}

-(PrefixDescriptorObject*) getPDO
{
	return pdobject;
}

-(NSString*) getWhere2Search
{
	return where2Search;
}

-(int) getSearchPosition
{
	return searchPosition;
}
@end



@implementation ChainsawEncoderProvider
{
	NSMutableArray *encoderDescriptors;
}

+(ChainsawEncoderProvider*) createDefaultEncoderProvider
{
        ChainsawEncoderProvider *res = [[ChainsawEncoderProvider alloc] init];
	
	[res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc] 
		initWithPrefix:IMAGE_PREFIX andDescription: @"Screenshot"]];
        [res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:CLICK_PREFIX andDescription: @"Click"]];
        [res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:DRAG_PREFIX andDescription: @"Drag"]];


        [res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:HOME_PREFIX andDescription: @"Home"]];


	[res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:SHOW_RUN_PREFIX andDescription: @"ShowRun"]];

	[res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:UNLOCK_PREFIX andDescription: @"Unlock"]];

	[res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:REBOOT_PREFIX andDescription: @"Reboot"]];



        [res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:ID_PREFIX andDescription: @"Id"]];
        [res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:SET_SCREENSHOT_SPEED_PREFIX andDescription: @"Scrspd"]];
        [res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:BATTERY_VALUE_PREFIX andDescription: @"Battery"]];
	[res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:WORKING_TIME_PREFIX andDescription: @"WorkingT"]];

	[res addPrefixDescriptorObject: [[PrefixDescriptorObject alloc]
                initWithPrefix:TEXT_PREFIX andDescription: @"Text"]];

	return res;
}

-(id) init
{
        self = [super init];
        encoderDescriptors = [[NSMutableArray alloc] initWithCapacity:1];
        return self;
}

-(int) getMaxReceiveObjectSize
{
	return MAX_RECEIVE_OBJECT_SIZE;
}


-(void) addPrefixDescriptorObject: (PrefixDescriptorObject*) pdo
{
	[encoderDescriptors addObject: pdo];
}

-(int) getMaxPrefixLen
{
	int maxLen = -1;

	for (int i = 0; i < [encoderDescriptors count]; i++)
	{
		PrefixDescriptorObject *pdo = [encoderDescriptors objectAtIndex:i];
		int curLen = [pdo getPrefix].length;
		
		if (maxLen < 0 || curLen > maxLen) 
		{
			maxLen = curLen;	
		}	
	}

	return maxLen;
}

-(int) getMinPrefixLen
{
	int minLen = -1;

        for (int i = 0; i < [encoderDescriptors count]; i++)
        {
                PrefixDescriptorObject *pdo = [encoderDescriptors objectAtIndex:i];
                int curLen = [pdo getPrefix].length;

                if (minLen < 0 || curLen < minLen)
                {
                        minLen = curLen;
                }
        }


	return minLen;
}

-(int) getSizeLen
{
	return SIZE_DEFAULT_SECTION;
}

-(int) getMinLength2Mantain
{
	return [self getMaxPrefixLen] + [self getSizeLen];
}
-(int) getPrefixCount
{
	return [encoderDescriptors count];
}
-(PrefixDescriptorObject*) getPrefixAtIndex: (int) index
{
	return [encoderDescriptors objectAtIndex: index];
}

-(ChainsawEncoderProviderSearchResult*) findPrefix: (NSString *) where;
{
	ChainsawEncoderProviderSearchResult *res = nil;

	int currentLocation = -1;
	int currentIndex = -1;
	PrefixDescriptorObject *currentPDO = nil;

	for (int i = 0; i < [encoderDescriptors count]; i++) {
		PrefixDescriptorObject *pdo = [encoderDescriptors objectAtIndex:i];
		NSRange range = [where rangeOfString: [pdo getPrefix]];
		
		if (range.location != NSNotFound && (currentLocation < 0 || currentLocation > range.location)) {
			currentIndex = i;
			currentLocation = range.location;	
			currentPDO = pdo;
		}
	}
	
	res = [[ChainsawEncoderProviderSearchResult alloc] initWithPDO: currentPDO andIndex: currentIndex andPosition: currentLocation andWhere: where];

	return res;
}

@end
