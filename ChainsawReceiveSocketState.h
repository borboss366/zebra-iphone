#import "ChainsawEncoder.h"
#import "ChainsawReceivedObject.h"

@interface ChainsawReceiveSocketState: NSObject

-(id) initWithChainsawEncoderProvider: (ChainsawEncoderProvider*) encoderProvider andNSData: (NSData*) data;
-(void) addNewData: (NSData*) newReadData;
-(int) getReadQuant;

-(NSString*) getCurrentPrefixString;

-(NSData*) getRemainingData;
-(BOOL) isFinished;
-(BOOL) isObjectParsed;
-(ChainsawReceivedObject*) getReceivedObject;
-(BOOL) isObjectTooLarge;
-(BOOL) isObjectFailed;

@end
