#import "ActivatorScrmaherSource.h" 

@implementation HomeScrmaherSource {
	NSString* _name;
}
 
static HomeScrmaherSource *homeDataSource;
static HomeScrmaherSource *rebootDataSource;
static HomeScrmaherSource *unlockDataSource;
static HomeScrmaherSource *showRunDataSource;
 
+ (void)load
{
	homeDataSource = [[HomeScrmaherSource alloc] initWithName:HOME_NAME];
	rebootDataSource = [[HomeScrmaherSource alloc] initWithName:REBOOT_NAME];
	unlockDataSource = [[HomeScrmaherSource alloc] initWithName:UNLOCK_NAME];
	showRunDataSource = [[HomeScrmaherSource alloc] initWithName:SHOW_RUN_NAME];
}
 
- (id)initWithName:(NSString *)name {
	if ((self = [super init])) {
		_name = name;
		[LASharedActivator registerEventDataSource:self forEventName:_name];
	}

	return self;
}
 
- (NSString *)localizedTitleForEventName:(NSString *)eventName {
        return [@"Title " stringByAppendingString: _name];
}
 
- (NSString *)localizedGroupForEventName:(NSString *)eventName {
        return @"Unlocking";
}
 
- (NSString *)localizedDescriptionForEventName:(NSString *)eventName {
        return [@"Description " stringByAppendingString: _name];
}
 
@end

