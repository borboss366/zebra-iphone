#import "Chainsaw.h"
#import "ChainsawEncoder.h"
#import "GCDAsyncSocket.h"

@interface ChainsawServer: NSObject<IGCDSocketServer>
- (id) initWithChainsawEncoder: (ChainsawEncoderProvider *) encoder;
- (id) initWithChainsawEncoder: (ChainsawEncoderProvider *) encoder andQueue: (dispatch_queue_t) queue;

@end

