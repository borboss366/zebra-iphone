#import "Chainsaw.h"
#import "ActionSender.h"


@interface ChainsawReceivedObjectCreator: NSObject<ActionSender>
-(void) setSocketServer: (id<IGCDSocketServer>) socketServer;
@end
