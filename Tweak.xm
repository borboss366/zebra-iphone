/* How to Hook with Logos
Hooks are written with syntax similar to that of an Objective-C @implementation.
You don't need to #include <substrate.h>, it will be done automatically, as will
the generation of a class list and an automatic constructor.


%hook ClassName

// Hooking a class method
+ (id)sharedInstance {
	return %orig;
}

// Hooking an instance method with an argument.
- (void)messageName:(int)argument {
	%log; // Write a message about this call, including its class, name and arguments, to the system log.

	%orig; // Call through to the original function with its original arguments.
	%orig(nil); // Call through to the original function with a custom argument.

	// If you use %orig(), you MUST supply all arguments (except for self and _cmd, the automatically generated ones.)
}

// Hooking an instance method with no arguments.
- (id)noArguments {
	%log;
	id awesome = %orig;
	[awesome doSomethingElse];

	return awesome;
}

// Always make sure you clean up after yourself; Not doing so could have grave consequences!
%end
*/

/*
 ActionMaher *maher = [[ActionMaher alloc] init];
        [maher setScreenshotSpeed: 500];

        ChainsawEncoderProvider *encoderProvider = [ChainsawEncoderProvider createDefaultEncoderProvider];

        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
        id <IGCDSocketServer> server = [[ChainsawServer alloc] initWithChainsawEncoder: encoderProvider andQueue: queue];

        ChainsawReceivedObjectCreator *objectCreator = [[ChainsawReceivedObjectCreator alloc] init];
        [objectCreator setSocketServer: server];
        [maher addActionSender: objectCreator];

        CommandsDecoder *decoder = [[CommandsDecoder alloc] init];
        CommandsImplementor *implementor = [[CommandsImplementor alloc] init];

        SocketServerAdapter *adapter = [[SocketServerAdapter alloc] initWithCommandsImplementor: implementor andDecoder: decoder];
        [server addServerListener: adapter];

*/

#import "ActionMaher.h"
#import "ChainsawSocket.h"
#import "ChainsawEncoder.h"
#import "CommandsImplementor.h"
#import "CommandsDecoder.h"
#import "ChainsawReceivedObjectCreator.h"
#import "SocketServerAdapter.h"
#import "ActivatorScrmaherSource.h"

static ActionMaher* maher;
static id <IGCDSocketServer> server;

%hook SpringBoard 	
- (void)applicationDidFinishLaunching:(id)application 
{
	%orig;

	[HomeScrmaherSource load];

	// now init the server and other stuff
	maher = [[ActionMaher alloc] init];
	[maher setScreenshotSpeed: 1000];
	
	ChainsawEncoderProvider *encoderProvider = [ChainsawEncoderProvider createDefaultEncoderProvider];

	dispatch_queue_t queue = dispatch_queue_create("implementor.queue", NULL);
	server = [[ChainsawServer alloc] initWithChainsawEncoder: encoderProvider andQueue: queue];

	ChainsawReceivedObjectCreator *objectCreator = [[ChainsawReceivedObjectCreator alloc] init];
	[objectCreator setSocketServer: server];
	[maher addActionSender: objectCreator];

	CommandsDecoder *decoder = [[CommandsDecoder alloc] init];
	CommandsImplementor *implementor = [[CommandsImplementor alloc] initWithActionImplementor: maher];
        
	SocketServerAdapter *adapter = [[SocketServerAdapter alloc] initWithCommandsImplementor: implementor andDecoder: decoder andMaher: maher];
	[server addServerListener: adapter];

	[server startSocketServer];
	[maher startWorking];

	

	UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"SCRMAHER v 1.3" 
		message:@"Message" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	[error show];
	//[NSThread sleepForTimeInterval: 5.0f];
	//[error dismissWithClickedButtonIndex:0 animated:NO];
	
} %end
