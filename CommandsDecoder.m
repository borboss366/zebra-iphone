#import "CommandsDecoder.h"
#import "Chainsaw.h"

@implementation CommandsDecoder
{
}

-(id<ICommandToImplement>) createCommandFromObject: (ChainsawReceivedObject*) object
{
	id<ICommandToImplement> res = nil;
	
	if (object) {
		NSString *prefix = [[object getPrefixDescriptorObject] getPrefix];
		
		if ([prefix isEqualToString:CLICK_PREFIX]) {
			res = [ScreenClick createFromString: [object getObjectString]];
		} else if ([prefix isEqualToString: DRAG_PREFIX]) {
			res = [DragClick createFromString: [object getObjectString]];
		} else if ([prefix isEqualToString: HOME_PREFIX]) {
			res = [HomeClick createFromString: [object getObjectString]];
		} else if ([prefix isEqualToString: SET_SCREENSHOT_SPEED_PREFIX]) {
			res = [ScreenshotDelay createFromString: [object getObjectString]];
		} else if ([prefix isEqualToString: REBOOT_PREFIX]) {
			res = [RebootIPhone createFromString: [object getObjectString]];
		} else if ([prefix isEqualToString: SHOW_RUN_PREFIX]) {
			res = [ShowRun createFromString: [object getObjectString]];
		} else if ([prefix isEqualToString: UNLOCK_PREFIX]) {
			res = [UnlockIPhone createFromString: [object getObjectString]];
		} else if ([prefix isEqualToString: TEXT_PREFIX]) {
			res = [TextInput createFromString: [object getObjectString]];
		}
	}

	return res;
}
@end

