#import "Commands.h"
#import "ChainsawReceivedObject.h"

@interface CommandsDecoder: NSObject 
-(id<ICommandToImplement>) createCommandFromObject: (ChainsawReceivedObject*) object;
@end



