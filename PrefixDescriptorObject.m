#import "PrefixDescriptorObject.h"


/*
@interface PrefixDescriptorObject
- (NSString*) getPrefix;
- (NSString*) description;
- (int) isPrefixWithLengthPresent: (NSString*) nsData;
@end

*/

@implementation PrefixDescriptorObject 
{
	NSString *prefixString;
	NSString *descriptionString;
}

- (id) initWithPrefix: (NSString*) prefix andDescription: (NSString*) description;
{
	self = [super init];	
	prefixString = prefix;
	descriptionString = description;
	return self;
}

- (NSString*) getPrefix 
{
	return prefixString;
}

- (NSData*) getPrefixData
{
	return [prefixString dataUsingEncoding:NSASCIIStringEncoding];
}

- (NSString*) getDescription
{
	return descriptionString;
}

- (NSRange) isPrefixWithLengthPresent: (NSString*) readString
{
	return [readString rangeOfString: prefixString];
}
@end


