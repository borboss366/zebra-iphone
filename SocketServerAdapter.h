#import "ChainsawSocket.h"
#import "CommandsImplementor.h"
#import "CommandsDecoder.h"
#import "ActionMaher.h"

@interface SocketServerAdapter: NSObject<IGCDSocketServerListener>
-(id) initWithCommandsImplementor: (CommandsImplementor*) impl andDecoder: (CommandsDecoder*) decoder andMaher: (ActionMaher*) maher; 
@end

