#import "libactivator/libactivator.h"

#define HOME_NAME @"ru.borman.scrmaher"
#define REBOOT_NAME @"ru.borman.scrmaher.reboot"
#define SHOW_RUN_NAME @"ru.borman.scrmaher.showrun"
#define UNLOCK_NAME @"ru.borman.scrmaher.unlock"

@interface HomeScrmaherSource: NSObject <LAEventDataSource>
+ (void)load;
@end
