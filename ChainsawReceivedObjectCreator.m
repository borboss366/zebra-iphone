#import "ChainsawReceivedObjectCreator.h"


@implementation ChainsawReceivedObjectCreator 
{
	id<IGCDSocketServer> server;
}

-(id) init
{
	self = [super init];
	server = nil;
	return self;
}

-(void) setSocketServer: (id<IGCDSocketServer>) socketServer 
{
	server = socketServer;
}

- (void) sendScreenshot: (UIImage *) screenshot
{
	if (screenshot) {
		NSString *prefix = IMAGE_PREFIX;
		NSData *imageData = UIImagePNGRepresentation(screenshot);
		[self processObjectData: prefix withData: imageData];
	}
}

- (void) sendBatteryLeft: (int) batteryLeft
{
        NSString *prefix = BATTERY_VALUE_PREFIX;
	NSString *encodedString = [NSString stringWithFormat: @"batleft,%d", batteryLeft];
	NSData *data = [encodedString dataUsingEncoding:NSASCIIStringEncoding];
	[self processObjectData: prefix withData: data];
}

- (void) sendId: (NSString*) idString
{
        NSString *prefix = ID_PREFIX;
        NSString *encodedString = [NSString stringWithFormat: @"id,(%@)", idString];
        NSData *data = [encodedString dataUsingEncoding:NSASCIIStringEncoding];
        [self processObjectData: prefix withData: data];
}

- (void) sendTimeSinceStarted: (long) timeSinceStarted
{
        NSString *prefix = WORKING_TIME_PREFIX;
	NSString *encodedString = [NSString stringWithFormat: @"wtime,%ld", timeSinceStarted];
        NSData *data = [encodedString dataUsingEncoding:NSASCIIStringEncoding];
        [self processObjectData: prefix withData: data];
}

-(void) processObjectData: (NSString *) prefix withData: (NSData*) data
{
        ChainsawReceivedObject *cro = [self createCRO: prefix withData: data];
        [self sendCRO: cro];
}

- (ChainsawReceivedObject*) createCRO: (NSString *) prefix withData: (NSData*) data
{
	PrefixDescriptorObject *pdo = [[PrefixDescriptorObject alloc] initWithPrefix:prefix andDescription: @"4sending"];
	return [[ChainsawReceivedObject alloc] initWithPrefix: pdo andObjectData: data];
}

- (void) sendRunningStateChanged: (BOOL) newRunningState
{
	// do nothing here
}

- (void) sendCRO: (ChainsawReceivedObject *) cro
{
	[server addObjectForSending: cro];
} 
@end
