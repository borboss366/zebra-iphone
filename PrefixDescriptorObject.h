@interface PrefixDescriptorObject: NSObject
- (id) initWithPrefix: (NSString*) prefix andDescription: (NSString*) description;
- (NSString*) getPrefix;
- (NSData*) getPrefixData;
- (NSString*) getDescription;
- (NSRange) isPrefixWithLengthPresent: (NSString*) nsData;
@end

