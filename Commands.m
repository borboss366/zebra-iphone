#import "Commands.h"

@implementation ScreenClick
{
        int xVal;
        int yVal;
}

-(id) initFromX: (int) x andY : (int) y
{
        self = [super init];
        xVal = x;
        yVal = y;
        return self;
}

-(int) getX
{
        return xVal;
}

-(int) getY
{
        return yVal;
}

-(NSString*) getDescription
{
        return [NSString stringWithFormat:@"CLICK %d:%d", xVal, yVal];
}

+(ScreenClick*) createFromString: (NSString*) str
{
        ScreenClick *res = nil;
        NSArray *vals = [str componentsSeparatedByString: @","];

        if (vals.count == 3) {
                int x = [[vals objectAtIndex:1] integerValue];
                int y = [[vals objectAtIndex:2] integerValue];
                res = [[ScreenClick alloc] initFromX: x andY: y];
        }

        return res;
}
@end


@implementation DragClick
{
        int xStart;
        int yStart;
        int xEnd;
        int yEnd;
        int delayMS;
}

-(id) initXFrom: (int) xFrom andYFrom : (int) yFrom andXTo : (int) xTo andYTo: (int) yTo andDurMS: (int) durMS
{
        self = [super init];

        xStart = xFrom;
        yStart = yFrom;
        xEnd = xTo;
        yEnd = yTo;
        delayMS = durMS;

        return self;
}


-(int) getXStart
{
        return xStart;
}

-(int) getYStart
{
        return yStart;
}

-(int) getXEnd
{
        return xEnd;
}

-(int) getYEnd
{
        return yEnd;
}

-(int) getDelayMS
{
        return delayMS;
}

+(DragClick*) createFromString: (NSString*) str
{
        DragClick *res = nil;
        NSArray *vals = [str componentsSeparatedByString: @","];

        if (vals.count == 6) {
                int xFrom = [[vals objectAtIndex:1] integerValue];
                int yFrom = [[vals objectAtIndex:2] integerValue];
                int xTo = [[vals objectAtIndex:3] integerValue];
                int yTo  = [[vals objectAtIndex:4] integerValue];
                int durationMS = [[vals objectAtIndex:5] integerValue];

                res = [[DragClick alloc] initXFrom: xFrom andYFrom: yFrom andXTo: xTo andYTo:yTo andDurMS: durationMS];
        }


        return res;
}

-(NSString*) getDescription
{
        return [NSString stringWithFormat:@"DRAG %d:%d->%d:%d %dms", xStart, yStart, xEnd, yEnd, delayMS];
}
@end



@implementation HomeClick
+(HomeClick*) createFromString: (NSString*) str
{
        HomeClick *res = nil;

        if ([str isEqualToString:@"home"]) {
                res = [[HomeClick alloc] init];
        }

        return res;
}

-(NSString*) getDescription
{
        return @"Home click";
}
@end

@implementation RebootIPhone
+(RebootIPhone*) createFromString: (NSString*) str
{
        RebootIPhone *res = nil;
        
        if ([str isEqualToString:@"reboot"]) {
                res = [[RebootIPhone alloc] init];
        }
        
        return res;
}

-(NSString*) getDescription
{
        return @"Reboot iphone";
}
@end


@implementation UnlockIPhone
+(UnlockIPhone*) createFromString: (NSString*) str
{
        UnlockIPhone *res = nil;
        
        if ([str isEqualToString:@"unlock"]) {
                res = [[UnlockIPhone alloc] init];
        }
        
        return res;
}

-(NSString*) getDescription
{
        return @"Unlock iphone";
}
@end

@implementation ShowRun
+(ShowRun*) createFromString: (NSString*) str
{
        ShowRun *res = nil;

        if ([str isEqualToString:@"showrun"]) {
                res = [[ShowRun alloc] init];
        }

        return res;
}

-(NSString*) getDescription
{
        return @"Show run";
}
@end


@implementation ScreenshotDelay
{
	int screenshotDelay;
}

-(id) initFrom: (int) delay
{
        self = [super init];
        screenshotDelay = delay;
        return self;
}

+(ScreenshotDelay*) createFromString: (NSString *)str
{
        ScreenshotDelay *res = nil;
        NSArray *vals = [str componentsSeparatedByString: @","];

        if (vals.count == 2) {
                int delay = [[vals objectAtIndex:1] integerValue];
		res = [[ScreenshotDelay alloc] initFrom: delay]; 
	}

	return res;
}

-(int) getDelayMillis
{
	return screenshotDelay;
}

-(NSString*) getDescription
{
        return [NSString stringWithFormat:@"SCRDELAY %d", screenshotDelay];
}
@end

@implementation TextInput
{
	NSString* text;
}

+(TextInput*) createFromString: (NSString *) str
{
	return [[TextInput alloc] initFrom: str];
}

-(id) initFrom: (NSString *) str
{
	self = [super init];
	text = str;
	return self;
}

-(NSString*) getDescription
{
        return [NSString stringWithFormat:@"TEXT %@", text];
}

-(NSString*) getText
{
	return text;
}
@end

