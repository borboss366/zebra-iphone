#import "ActionMaher.h"
#import "FighterUtils.h"

@implementation ActionMaher {
	int screenshotSpeed;
	
	long firstTimeMeasure;

	long lastBatterySyncTime;

	BOOL syncIdASAP;
	long lastIdSyncTime;

	long lastSinceStartedSyncTime;	

	// the timer for the actions
	BOOL working;
	
	dispatch_queue_t dispatchQueue;
	dispatch_source_t dispatchTimer;
	//NSTimer *screenshotTimer;

	NSMutableArray *actionSenders;
}

- (void) syncIdASAP
{
	syncIdASAP = YES;
}

- (BOOL) timePassed: (long) timeStarted : (long) period
{
	long current = [self getCurrentTime];
	long delta = current - timeStarted;
	
	return delta > period;
}

-(long) getCurrentTime
{
	return [FighterUtils getCurrentTimeMS];
}

-(id) init
{
	self = [super init];
	if (self) 
	{
		working = NO;
		long currentTime = [self getCurrentTime];

		firstTimeMeasure = currentTime;
		screenshotSpeed = (MIN_SCREENSHOT_SPEED + MAX_SCREENSHOT_SPEED) / 2;
		
		lastBatterySyncTime = currentTime;
		lastIdSyncTime = currentTime;
		syncIdASAP = NO;
		lastSinceStartedSyncTime = currentTime;
		dispatchQueue = [self getDispatchQueue];
		
		actionSenders = [[NSMutableArray alloc] initWithCapacity:1];
	}

	return self;
}

-(BOOL) isWorking
{
	return working;
}

- (dispatch_queue_t) getDispatchQueue
{
	// can be replaced by the dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	// this is a system background thread
	//return dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	//return dispatch_get_main_queue();
	return dispatch_queue_create("maher.queue", NULL);
}

- (dispatch_source_t) createDispatchTimer: (dispatch_queue_t) queue  : (dispatch_block_t) block
{
	dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
	
	if (timer)
	{
		// sets the timer

		[self updateTimerDelay: timer];
		
		// so the timer fires in the block
		dispatch_source_set_event_handler(timer, block);
		// So that the timer can process the blocks weiter
		dispatch_resume(timer);
	}

	return timer;
}

- (void) updateTimerDelay: (dispatch_source_t) timer
{	
	float interval = [self getFloatDelay];
	dispatch_source_set_timer(timer, 
		dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), 
		interval * NSEC_PER_SEC, // interval is set in NANO sec 
		(1ull * NSEC_PER_SEC) / 10); // unsigned mat ego long long	
}

- (void) startWorking
{
	if (!working) {
		//dispatch_queue_t queue = [self getDispatchQueue];
		dispatchTimer = [self createDispatchTimer: dispatchQueue: ^{ [self timerHappened]; }];
		working = YES;
		[self updateRunningStateChanged];
	}
}

- (float) getFloatDelay
{
	return (screenshotSpeed + 0.0) / 1000;
}

- (void) stopWorking
{
	if (working) {
		dispatch_source_cancel(dispatchTimer);
		working = NO;
		[self updateRunningStateChanged];
	}
}

- (void) addActionSender: (id<ActionSender>) as
{
	@synchronized(actionSenders) 
	{
		[actionSenders addObject: as];
	}
}

- (void) setScreenshotSpeed: (int) speed 
{
	if (speed < MIN_SCREENSHOT_SPEED) {
		speed = MIN_SCREENSHOT_SPEED;
	} 

	if (speed > MAX_SCREENSHOT_SPEED) {
		speed = MAX_SCREENSHOT_SPEED;
	}


	if (screenshotSpeed != speed) {
		[self updateSpeed: speed];
	}

	
}

- (int) getScreenshotSpeed
{
	return screenshotSpeed;
}

- (void) updateSpeed: (int) newSpeed
{
	screenshotSpeed = newSpeed;

	if (working) {
		[self updateTimerDelay: dispatchTimer];
	}
}

- (void) timerHappened
{
	[self sendScreenshot];
	[self updateUnnecessaryParams];
}

- (void) sendScreenshot
{
        @try {
		CGImageRef imgRef = [FighterUtils captureMyScreen];
		UIImage *image = [UIImage imageWithCGImage:imgRef];
		
		/*
		if (!image && imgRef) 
		{
                	UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"ddd"
                		message:@"strage img" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                	[error show];
		}
		*/

		if (image) {
			//NSString *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.png"];
			//[UIImagePNGRepresentation(image) writeToFile:pngPath atomically:YES];
                        [self sendScreenshotInSender: image];
		}

		CGImageRelease(imgRef);
        }
        @catch(NSException *e) {

        }
}


- (void) sendScreenshotInSender: (UIImage*) screenshot
{
	@synchronized(actionSenders) 
	{
		for (int i = 0; i < [actionSenders count]; i++)
		{
			[[actionSenders objectAtIndex:i] sendScreenshot: screenshot];
		}
	}
}
- (void) updateUnnecessaryParams
{
	[self updateBattery];
	[self updateTimeSinceLaunch];
	[self updateId];
}

- (void) updateRunningStateChanged
{
	@synchronized(actionSenders)
        {
                for (int i = 0; i < [actionSenders count]; i++)
                {
                        [[actionSenders objectAtIndex:i] sendRunningStateChanged: working];
                }
        }
}

- (BOOL) isNeedToUpdateBattery
{
	return [self timePassed: lastBatterySyncTime: BATTERY_SPEED];
}

- (void) updateBattery
{
	if ([self isNeedToUpdateBattery]) {
		int battery = [FighterUtils getBatteryChargeValue];
		[self sendBatteryInSender: battery];
		//[actionSender sendBatteryLeft: battery];
		lastBatterySyncTime = [self getCurrentTime];
	}	
}

- (void) sendBatteryInSender: (int)batteryValue
{
	@synchronized(actionSenders)
        {
                for (int i = 0; i < [actionSenders count]; i++)
                {
                        [[actionSenders objectAtIndex:i] sendBatteryLeft: batteryValue];	
		}
	}
}

- (BOOL) isNeedToUpdateTimeSinceLaunch
{
	return [self timePassed: lastSinceStartedSyncTime: TIME_PASSED_SPEED];
}

- (long) getTimeSinceLaunch
{
	return [self getCurrentTime] - firstTimeMeasure;
}

- (void) updateTimeSinceLaunch
{
	if ([self isNeedToUpdateTimeSinceLaunch]) {
		//[actionSender sendTimeSinceStarted: [self getTimeSinceLaunch]];
		[self sendTimeSinceStartedInSender: [self getTimeSinceLaunch]];
		lastSinceStartedSyncTime = [self getCurrentTime];
	}
}

- (void) sendTimeSinceStartedInSender: (long) time
{
        @synchronized(actionSenders)
        {
                for (int i = 0; i < [actionSenders count]; i++)
                {
                        [[actionSenders objectAtIndex:i] sendTimeSinceStarted: time];   
                }
        }
}

- (BOOL) isNeedToUpdateId
{
	return [self timePassed: lastIdSyncTime: ID_SPEED] || syncIdASAP;
}

- (void) updateId
{
	if ([self isNeedToUpdateId]) {
		NSString *id = [FighterUtils getIPhoneId];
		if (id != nil) {
			[self sendIdInSender: id];
			//[actionSender sendId: id];
		}
		lastIdSyncTime = [self getCurrentTime];	
		syncIdASAP = NO;
	}
}

- (void) sendIdInSender: (NSString*) id
{
        @synchronized(actionSenders)
        {
                for (int i = 0; i < [actionSenders count]; i++)
                {
                        [[actionSenders objectAtIndex:i] sendId: id];        
                }
        }

}

@end
